package edu.logging;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class A8 {

    private static Logger logger = LogManager.getLogger(A8.class);

    public A8() {
        logger.fatal("This is fatal message; it will be sent via SMS");
    }
}