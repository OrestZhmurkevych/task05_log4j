package edu.logging;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class A5 {

    private static Logger logger = LogManager.getLogger(A5.class);

    public A5() {
        logger.trace("This is trace message; all levels higher than warn will be saved in the file");
        logger.debug("This is debug message; all levels higher than warn will be saved in the file");
        logger.info("This is info message; all levels higher than warn will be saved in the file");
        logger.warn("This is warning message; all levels higher than warn will be saved in the file");
        logger.error("This is error message; all levels higher than warn will be saved in the file");
        logger.fatal("This is fatal message; all levels higher than warn will be saved in the file");
    }
}