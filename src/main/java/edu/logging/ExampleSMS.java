package edu.logging;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {

    public static final String ACCOUNT_SID = "AC7ee129ec7d4036215ccfb6d95ca78155";
    public static final String AUTH_TOKEN = "495d0e462574a2d8b860ad384d002cd6";

    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                .creator(new PhoneNumber("+380986261730"), /*my phone number*/
                        new PhoneNumber("+12018498480"), str) .create(); /*attached to me number*/
    }
}