package edu.logging;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class A6 {

    private static Logger logger = LogManager.getLogger(A6.class);

    public A6() {
        logger.info("This is info message; it will be recorded only at the console");
        logger.warn("This is warning message; it will be recorded only in the file");
    }
}