package edu.logging;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class A4 {

    private static Logger logger = LogManager.getLogger(A4.class);

    public A4() {
        logger.error("This is error message; every day log will be written to a new file");
    }
}