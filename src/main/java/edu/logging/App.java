package edu.logging;

import org.apache.logging.log4j.*;

public class App {

    private static Logger logger = LogManager.getLogger(App.class);

    public static void main(String[] args) {
        logger.debug("This is debug message");
//        logger.info("This is info message");
        A1 first = new A1();
        A2 second = new A2();
        A3 third = new A3();
        A4 forth = new A4();
        A5 fifth = new A5();
        A6 sixth = new A6();
        A7 seventh = new A7();
        A8 eighth = new A8();
    }
}
